import numpy as np
from matplotlib import animation
import matplotlib.pyplot as pl
from cycler import cycler


# initialization function: plot the background of each frame
def init():    
    for line in lines:
        line.set_data([], [])
    return lines


# animation function.  This is called sequentially
def animate_from_list(i, *args):
    x_list, y_list = args
    xi, yi = x_list[i], y_list[i]

    # Setting the second line as current sol
    lines[1].set_data(xi, y_list[0])

    # Setting the first line as init sol
    lines[0].set_data(xi, yi)

    return lines


def built_lists(n):
    x = []
    y = []
    for i in xrange(n):
        x_tmp = np.linspace(0., 2. * np.pi, 100)
        y_tmp = np.sin((i * 2. * np.pi / 100.)) * np.sin(x_tmp)
        x.append(x_tmp)
        y.append(y_tmp)
        # print np.max(i*np.sin(x))
    return x, y


def nice_axis(ax, x, y):
    x_min = np.amin(x)
    y_min = np.amin(y)
    x_max = np.amax(x)
    y_max = np.amax(y)

    ax = pl.axes(xlim=(x_min, x_max), ylim=(y_min, y_max))
    pl.tight_layout()

    return ax


def get_tex_layout():
    from matplotlib import rcParams
    rcParams['text.usetex'] = True
    rcParams['mathtext.fontset'] = 'stix'
    rcParams['font.family'] = 'STIXGeneral'

    rc_params = {
        'axes.labelsize': 30,
        'xtick.labelsize': 30,
        'ytick.labelsize': 30,
        'font.size': 30,
        'legend.fontsize': 30
    }

    pl.rc('lines', linewidth=4)

    use_pl_colormap = False

    if use_pl_colormap:
        cut_end = False
        nb_lines = 10

        # chosen_colormap = pl.cm.jet
        chosen_colormap = pl.cm.bwr
        chosen_colormap = pl.cm.autumn

        color_list = []

        for lne in np.linspace(0, 1, nb_lines + 2):
            if cut_end:
                if lne != 0 or lne != nb_lines + 2:
                    color_list.append(chosen_colormap(lne))
            else:
                color_list.append(chosen_colormap(lne))

    else:
        color_list = ['Black', 'Red']

    line_style_list = ['-', '--']
    line_width_list = [1.5, 0.75]

    clr_list = []
    ls_list = []
    lw_list = []

    for lw in line_width_list:
        for ls in line_style_list:
            for clr in color_list:
                clr_list.append(clr)
                ls_list.append(ls)
                lw_list.append(lw)

    pl.rc('axes', prop_cycle=(cycler('color', ['k', 'r']) +
                              cycler('linestyle', ['-', '--']) +
                              cycler('lw', [1.5, 2.])
                              )
          )

    rcParams.update(rc_params)

    return None



def animate_plots(x=None, y=None, video_title='Default_Name_For_Video', xlabel='', ylabel=''):
    """
    x and y are lists containing the data to be ploted in time
    """
    get_tex_layout()
    # ------------------------------------------------------------------------------------
    # Animate

    # First set up the figure, the axis, and the plot element we want to animate
    fig = pl.figure()
    ax = pl.axes(xlim=(0, 0), ylim=(0, 0))
    ax = nice_axis(ax, x, y)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Setting number of lines
    N = 2
    lines = [pl.plot([], [], lw=3)[0] for _ in range(N)]

    #line = ax.plot([], [], lw=2)
    global lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate_from_list, init_func=init, fargs=(x, y),
                                   frames=len(x), interval=20, blit=True, repeat_delay=2000.)

    # save the animation as an mp4.  This requires ffmpeg or mencoder to be
    # installed.  The extra_args ensure that the x264 codec is used, so that
    # the video can be embedded in html5.  You may need to adjust this for
    # your system: for more information, see
    # http://matplotlib.sourceforge.net/api/animation_api.html
    anim.save(video_title + '.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

    #pl.show()

    return None
