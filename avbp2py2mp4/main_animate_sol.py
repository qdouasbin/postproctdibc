import animate_from_list as anim
import numpy as np
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as tick
import matplotlib
from matplotlib import rcParams

from params import *

def unpack_bin(npbin=None, name_lst=[]):
    out = []
    
    for var in name_lst:
        out.append(npbin[var])

    return tuple(out)

if __name__ == "__main__":
    # Loading binary data
    data_bin_file = tmp_bin_file + '.npz'
    data = np.load(data_bin_file)

    print data.files


    if False:
        x, u, p = unpack_bin(data, data.files)
    else:
        x = data['x']
        p = data['p']
        u = data['u']
        t = data['t']

    # Defining data to be plotted
    print """Mean of fluctuations :
                                    Pressure = %0.5e
                                    Velocity = %0.5e
""" % (np.mean(p), np.mean(u))
    pp = p - np.mean(p)
    up = u - np.mean(u)

    # Animate pressure
    anim.animate_plots(x=x, y=pp, video_title='pressure', xlabel='$x$ [$m$]', ylabel="$p{'}$ [$Pa$]")
    
    # Animate velocity
    anim.animate_plots(x=x, y=up, video_title='velocity', ylabel="$u^{'}$ [$m.s^{-1}$]",xlabel='$x$ [$m$]')
    
    print "\nDone.\n"
