from antares import *
import numpy as np
import pprint as pprint
#import ppretty as ppretty

from params import *

def fluct(somebase, zone, inst, var):
    print """
    Calling function fluct with:
        Zone        = %s
        Inst        = %s
        Var         = %s
""" % (zone, inst, var)

    print """
    The base has the following keys:
    """
    print somebase
    print somebase[0]
    print somebase[0][0]

    mean = np.mean(somebase[:][:][var])
    fluctuation = somebase[zone][inst][var] - mean
    return fluctuation
    

if __name__ == "__main__":
    verbose(2)

    r=Reader('hdf_avbp')
    r['filename'] = meshpath
    r['shared'] = True
    base=r.read()

    print base[0]

    # ------------------
    # Reading the files
    # ------------------
    reader = Reader('hdf_avbp')
    reader['base'] = base
    reader['filename'] = solpath
    base = reader.read()

    t = []
    for zone in base:
        for inst in base[zone]:
            current_time = base[zone][inst].attrs['dtsum']  
            print current_time
            t.append(current_time)

    # ------------------
    # Define line
    # ------------------
    nb = number_of_points_interp
    p1 = [-1.0, 0., 0.]
    p2 = [ 1.0, 0., 0.]

    # ------------------------
    # Interpolate on the line
    # ------------------------
    line = Treatment('line')
    line['base'] = base
    line['point1'] = p1
    line['point2'] = p2
    line['nbpoints'] = nb
    line['memory_mode'] = True
    intp_base = line.execute()

    print 'printing newbase'
    print intp_base
    print intp_base[0]
    print intp_base[0][0]
    print 'Done printing newbase'



    x = []
    p = []
    u = []

    for zone in intp_base:
        for inst in intp_base[zone]:
            # Data for computation
            rhou_b = intp_base[zone][inst]['rhou']
            rho_b = intp_base[zone][inst]['rho']
            u_b =  rhou_b / rho_b

            p_b = intp_base[zone][inst]['pressure']

            # Outputing data                
            x.append(intp_base[zone][inst]['x'])
            p.append(p_b)
            u.append(u_b)

    pp = np.array(p) - np.mean(p)    
    up = np.array(u) - np.mean(u)    


    out_variables = x, pp, up, t

    print "print shape of x"
    print np.shape(x)
    print "print shape of pp"
    print np.shape(pp)
    print "print shape of up"
    print np.shape(up)
    print "print shape of t"
    print np.shape(t)

    # Saving data as binary using numpy
    np.savez(tmp_bin_file, x=x, p=pp, u=up, t=t)

