#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as tick
import matplotlib
from matplotlib import rcParams

from params import *

rcParams['text.usetex'] = True
rcParams['text.latex.unicode'] = True
params1 = {
    'axes.labelsize'    : 20,
    'font.size'         : 20,
    'xtick.labelsize'   : 23,
    'ytick.labelsize'   : 20,
    'lines.markersize'  : 10,
    'font.size'         : 20,
    'legend.fontsize'   : 16
}
rcParams.update(params1)
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'


# --------------------------------------
# Functions
def load_AVBP_text_info(path):
    """
    Reads data in .txt format outputed by AVBP tdimp.f90
    """
    print 'Loading %s' % path
    data = np.loadtxt(path, skiprows=0)
    return data[::1]


def get_yMinMax(y, marginFactor):
    out = np.abs(np.max(np.amin(y), np.amax(y)))
    return out * marginFactor
# --------------------------------------


for patch_nb in patch_str_list:
    # --------------------------------------
    # LOADIND DATA
    print '\nReading data\n'
    conv = load_AVBP_text_info('../../tdibcOutput_' + patch_nb  + '_conv_int_value_iter.txt')
    totconv = load_AVBP_text_info('../../tdibcOutput_' + patch_nb  + '_totconv_int_value_iter.txt')
    dW_in = load_AVBP_text_info('../../tdibcOutput_' + patch_nb  + '_dW_in.txt')
    dW_out = load_AVBP_text_info('../../tdibcOutput_' + patch_nb  + '_dW_out.txt')

    # Time Output time for only one RK time step
    dt = load_AVBP_text_info('../../tdibcOutput_' + patch_nb  + '_dt.txt')
    # --------------------------------------

    # --------------------------------------
    # Useful variables
    ite = 0.5 * np.array(range(len(conv)))
    rho_0 = 1.13798
    a_0 = 352.8957
    # --------------------------------------

    #--------------------------------------
    # Waves Poinsot's Style
    L_in = dW_in / dt
    L_out = dW_out / dt
    L_conv = conv / dt
    L_cumsum_conv = np.cumsum(conv) / dt
    #--------------------------------------


    # --------------------------------------
    #  ACOUTSIC ENERGY 
    print '\nAcoustic variables computation'
    # Should be -1 in AVBP but we reason in term of acoustic flux :
    #   Leaving the domain  = -1
    #   Entering the domain = 1
    nx = 1. 

    uprim = 0.5 * nx * (np.cumsum(dW_in) - np.cumsum(dW_out))
    pprim = 0.5 * rho_0 * a_0 * (np.cumsum(dW_in) + np.cumsum(dW_out))
    AcousticEnergy = uprim * pprim * dt

    dt_sum_ms = 1e3 * np.cumsum(dt)
    dt_ms = 1e3 * dt

    np.savez(tmp_bin_file, convol=conv, dW_in=dW_in, dW_out=dW_out, dt=dt, ite=ite, rho_0=rho_0, a_0=a_0, nx=nx, up=uprim, pp=pprim, e_ac=AcousticEnergy, dt_sum_ms=dt_sum_ms, dt_ms=dt_ms, L_in=L_in, L_out=L_out, L_conv=L_conv) 

    if True:
        max_L = np.amax(np.abs(np.concatenate([L_in, L_out])))
        fig0 = pl.figure()
        ax0 = fig0.add_subplot(111)
        ax0.plot(dt_sum_ms, L_out, linewidth=2.0, linestyle='solid' , color='black', label='$\mathcal{L}_{\\textnormal{out}}$')
        ax0.plot(dt_sum_ms, L_in , linewidth=2.0, linestyle='dashed', color='black', label='$\mathcal{L}_{\\textnormal{in}}$')
        ax0.plot(dt_sum_ms, L_conv , linewidth=2.0, linestyle='dashed', color='red', label='$\mathcal{L}_{\\textnormal{conv}}$')
        ax0.set_xlabel('Time [ms]')
        ax0.set_xlim(np.amin(0.), np.amax(dt_sum_ms))
        pl.grid(which='major')
        pl.grid(which='minor')
        pl.tight_layout()
        pl.legend(loc='best')
        fig0.savefig('./' + patch_nb + '_L_in_out' + '.pdf', transparent=True, dpi=400)


    if True:
        max_dW = np.amax(np.abs(np.concatenate([dW_in, dW_out])))
        fig0 = pl.figure()
        ax0 = fig0.add_subplot(111)
        ax0.plot(dt_sum_ms, np.cumsum(dW_out), linewidth=2.0, linestyle='solid' , color='black', label='$W_{\\textnormal{out}}$')
        ax0.plot(dt_sum_ms, np.cumsum(dW_in) , linewidth=2.0, linestyle='dashed', color='black', label='$W_{\\textnormal{in}}$')
        ax0.plot(dt_sum_ms, np.cumsum(totconv), linewidth=2.0, linestyle='dashed', color='red', label='$\mathcal{\\partial}_{\\textnormal{totconv}}$')
        ax0.set_xlabel('Time [ms]')
        ax0.set_xlim(np.amin(0.), np.amax(dt_sum_ms))
        pl.grid(which='major')
        pl.grid(which='minor')
        pl.tight_layout()
        pl.legend(loc='best')
        fig0.savefig('./' + patch_nb + '_dW_in_out' + '.pdf', transparent=True, dpi=400)

    if True:
        fig0 = pl.figure()
        ax0 = fig0.add_subplot(111)
        ax0.plot(dt_sum_ms, dW_out, linewidth=2.0, linestyle='solid' , color='black', label= '${\\partial W}_{\\textnormal{out}}$')
        ax0.plot(dt_sum_ms, dW_in , linewidth=3.0, linestyle='dotted', color='blue', label='${\\partial W}_{\\textnormal{in}}$')
        ax0.plot(dt_sum_ms, totconv, linewidth=2.0, linestyle='dashed', color='red', label= '${\\partial W}_{\\textnormal{totconv}}$')
        ax0.set_xlabel('Time [ms]')
        ax0.set_xlim(np.amin(0.), np.amax(dt_sum_ms))
        pl.grid(which='major')
        pl.grid(which='minor')
        pl.tight_layout()
        pl.legend(loc='best')
        fig0.savefig('./' + patch_nb + '_dW_in_out' + '.pdf', transparent=True, dpi=400)

    if True:
        fig0 = pl.figure()
        ax0 = fig0.add_subplot(111)
        ax0.plot(dt_sum_ms, totconv, linewidth=2.0, linestyle='solid', color='black', label= '${\\partial W}_{\\textnormal{totconv}}$')
        ax0.plot(dt_sum_ms, np.cumsum(totconv), linewidth=2.0, linestyle='dashed', color='red', label= '${\\sum \\partial W}_{\\textnormal{totconv}}$')
        ax0.set_xlabel('Time [ms]')
        ax0.set_xlim(np.amin(0.), np.amax(dt_sum_ms))
        pl.grid(which='major')
        pl.grid(which='minor')
        pl.tight_layout()
        pl.legend(loc='best')
        fig0.savefig('./' + patch_nb + '_dW_in_out' + '.pdf', transparent=True, dpi=400)
        pl.show()

    fig0 = pl.figure()
    ax0 = fig0.add_subplot(111)
    ax0.set_title("$\Delta t$")
    ax0.plot(dt_sum_ms, dt, linewidth=2.0, color='black')
    ax0.set_ylabel('Timestep [s]')
    ax0.set_xlabel('Time [ms]')
    ax0.set_xlim(np.amin(0.), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()

    print 'Plotting uprim'
    fig1 = pl.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(dt_sum_ms, uprim, linewidth=2.0, color='black')
    ax1.set_xlabel('Time [ms]')
    ax1.set_ylabel(r"$u^{'}$ [$m.s^{-1}$]")
    ax1.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()

    print 'Plotting pprim'
    fig2 = pl.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(dt_sum_ms, pprim, linewidth=2.0, color='black')
    ax2.set_xlabel('Time [ms]')
    ax2.set_ylabel(r"$p^{'}$ [Pa]")
    ax2.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()

    print 'Plotting Acoustic Energy'
    fig3 = pl.figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(dt_sum_ms, AcousticEnergy, linewidth=2.0, color='black')
    ax3.set_title(r'Acoustic Energy Flux')
    ax3.set_xlabel('Time [ms]')
    ax3.set_ylabel(' $\\textnormal{E}_{\\textnormal{ac}}$ [J.m$^{-2}$] ')
    ax3.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()

    print 'Plotting Cumulative Acoustic Energy'
    fig4 = pl.figure()
    ax4 = fig4.add_subplot(111)
    ax4.plot(dt_sum_ms, np.cumsum(AcousticEnergy), linewidth=2.0, color='black')
    ax4.set_title(r'Total Acoustic Energy Flux')
    ax4.set_ylabel('$\\Delta \\textnormal{E}_{\\textnormal{ac}}$ [J.m$^{-2}$]')
    ax4.set_xlabel('Time [ms]')
    ax4.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()
    # --------------------------------------


    # --------------------------------------
    # Convolution Integral 

    print 'Plotting convolution integral'
    fig5 = pl.figure()
    ax5 = fig5.add_subplot(111)
    ax5.plot(dt_sum_ms, conv, linewidth=2.0, color='black', label=r'Convolution Integral')
    ax5.set_title(r'Value of Convolution integral')
    ax5.set_ylabel(r'Conv [-]')
    ylimits = get_yMinMax(conv, 1.2)
    ax5.set_ylim(-ylimits, ylimits)
    ax5.set_xlabel('Time [ms]')
    ax5.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
    pl.grid(which='major')
    pl.tight_layout()

    # Setting y axis format to exponential

    # xaxis
    if False:
        x_fmt = tick.FormatStrFormatter('%1.1f')
        ax0.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        ax1.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        ax2.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        ax3.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        ax4.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        ax5.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
        pl.tight_layout()

        # yaxis
        y_fmt = tick.FormatStrFormatter('%1.0f')
        #ax0.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        ax1.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        ax2.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        ax3.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        ax4.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        ax5.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        pl.tight_layout()
        # pl.grid(which='major')

    print "\nSaving plot\n"
    fig0.savefig('./' + patch_nb + '_Timestep' + '.pdf', transparent=True, dpi=400)
    fig1.savefig('./' + patch_nb + '_AcousticVelocity' + '.pdf', transparent=True, dpi=400)
    fig2.savefig('./' + patch_nb + '_AcousticPressure' + '.pdf', transparent=True, dpi=400)
    fig3.savefig('./' + patch_nb + '_AcousticPower' + '.pdf', transparent=True, dpi=400)
    fig4.savefig('./' + patch_nb + '_TotalAcousticPower' + '.pdf', transparent=True, dpi=400)
    fig5.savefig('./' + patch_nb + '_ConvolutionIntegral' + '.pdf', transparent=True, dpi=400)
    # --------------------------------------

if plot_show:
    pl.show()

print "\nEnd of the script\n"
